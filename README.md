## Web project 

##### Client Side web

###### Description: 
Consume Restful API to show the content by JSP

###### Prerequisite: 
* Run server side API project before and replace the host and port in config 
* The config under ``` src/main/resources/application.properties ```  .
for api.url 
need to be changed if API is running in different host or port

###### System Requirment: 
* Project require Java 8

###### Reference: 
Project is developed using Spring boot 1.3.5

###### Quick run project: 
``` 
./gradlew bootRun 
```
Notice: Client will be run under port: 7700, you can modify it by overriding the config.

###### Build deployable war:
``` 
./gradlew clean build 
```
The war file will be in ``` build/libs/client-side.war ```

###### Test:
``` ./gradlew clean test ```

###### Generate Eclipse project:
``` ./gradlew clean eclipse ```

###### Example endpoints:
* localhost:7700/setup