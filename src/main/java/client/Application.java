package client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.builder.SpringApplicationBuilder;  

/** 
 * App bootstrap spring 
 * 
 * @author Tong Zhou
 * @version 
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer 
{

    /** 
     * main to start spring 
     * 
     * @param args 
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception 
    {
        SpringApplication.run(Application.class, args);
    }

    
    /** 
     * config loader
     * 
     * @param application 
     * @return 
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) 
    {
        return application.sources(Application.class);
    }
}
