package client.controller;

import java.util.Map;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.RequestMethod;

import client.domain.Customer;
import client.domain.Order;
import client.domain.Product;
import client.response.Mapper;

/** 
 * Setup controller  
 * 
 * @author Tong Zhou
 * @version 
 */
@Controller
@RequestMapping(value = "/setup")
public class SetupController
{
    @Value("${application.message}")
    private String message;

    @Value("${api.url}")
    private String apiUrl;

    @Value("${api.endpoint.setup.customer}")
    private String apiEndpointSetupCustomer;

    @Value("${api.endpoint.setup.product}")
    private String apiEndpointSetupProduct;

    @Value("${api.endpoint.setup.order}")
    private String apiEndpointSetupOrder;

    @Value("${api.endpoint.customer}")
    private String apiEndpointCustomer;

    @Value("${api.endpoint.product}")
    private String apiEndpointProduct;

    @Value("${api.endpoint.order}")
    private String apiEndpointOrder;

    
    /** 
     * init page   
     * 
     * @param model 
     * @param model 
     * @return 
     */
    @RequestMapping(method = RequestMethod.GET)
    public String init(Map<String, Object> model)
    {
        model.put("greetingMsg", "Hello my friend:");
        return "setup";
    }

    
    /** 
     * setup done
     * 
     * @param model 
     * @param model 
     * @return 
     * @throws Exception 
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value="/done", method = RequestMethod.GET)
    public String done(Map<String, Object> model) throws Exception 
    {
        RestTemplate restTemplate = new RestTemplate();
        Mapper<Boolean> customerSetup = restTemplate.getForObject(apiUrl + apiEndpointSetupCustomer, Mapper.class);
        Mapper<Boolean> productSetup  = restTemplate.getForObject(apiUrl + apiEndpointSetupProduct, Mapper.class);
        Mapper<Boolean> orderSetup    = restTemplate.getForObject(apiUrl + apiEndpointSetupOrder, Mapper.class);

        if (customerSetup.getContent() && 
            productSetup.getContent() && 
            orderSetup.getContent()
        ) {
            Mapper<List<Customer>> customers = restTemplate.getForObject(apiUrl + apiEndpointCustomer, Mapper.class);
            Mapper<List<Product>> products  = restTemplate.getForObject(apiUrl + apiEndpointProduct, Mapper.class);
            Mapper<List<Order>> orders    = restTemplate.getForObject(apiUrl + apiEndpointOrder, Mapper.class);
            model.put("customers", customers.getContent());
            model.put("products", products.getContent());
            model.put("orders", orders.getContent());
            return "done";
        }

        throw new Exception("Database setup failed");
    }

} 
