<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Setup finished</title>
</head>
<body>
    <center><h4>List of Customers</h4></center>
    <table border="1" align="center" style="width:50%">
        <thead>
            <tr>
                <th>Customer Id</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="customer" items="${customers}" >
                <tr>
                    <td>${customer.id}</td>
                    <td>${customer.firstName}</td>
                    <td>${customer.lastName}</td>                   
                </tr>
            </c:forEach> 
        </tbody>
    </table> 

    <center><h4>List of Products</h4></center>
    <table border="1" align="center" style="width:50%">
        <thead>
            <tr>
                <th>Product Id</th>
                <th>Title</th>
                <th>Description</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="product" items="${products}" >
                <tr>
                    <td>${product.id}</td>
                    <td>${product.title}</td>
                    <td>${product.description}</td>
                    <td>${product.price}</td>                   
                </tr>
            </c:forEach> 
        </tbody>
    </table> 

    <center><h4>List of Orders</h4></center>
    <table border="1" align="center" style="width:50%">
        <thead>
            <tr>
                <th>Order Id</th>
                <th>Customer Id</th>
                <th>Total Price</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="order" items="${orders}" >
                <tr>
                    <td>${order.id}</td>
                    <td>${order.customerId}</td>
                    <td>${order.totalPrice}</td>                   
                </tr>
            </c:forEach> 
        </tbody>
    </table> 
</body>
</html>
