<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Setup finished</title>
</head>

<body>
    <c:url value="/setup/done" var="url"/>
    <spring:url value="/setup/done" htmlEscape="true" var="setupUrl" />
    <h3>${greetingMsg}</h3>
    <h4><a href="${url}">Please click the line to setup tables</a></h4>
</body>

</html>
